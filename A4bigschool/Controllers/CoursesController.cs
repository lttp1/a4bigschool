﻿using A4bigschool.Models;
using A4bigschool.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace A4bigschool.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public CoursesController()
        {
            _dbContext = new ApplicationDbContext();

        }
        public ActionResult Create()
        {
            var viewModel = new CourseViewMode
            {
                Categories = _dbContext.Categories.ToList()
            };
            return View(viewModel);
        }


        // GET: Courses
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CourseViewMode viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Categories = _dbContext.Categories.ToList();
                return View("Create", viewModel);
            }
            var course = new Course
            {
                LecturerId = User.Identity.GetUserId(),
                DateTime = viewModel.GetDateTime(),
                CategoryId = viewModel.Category,
                Place = viewModel.Place,

            };
            _dbContext.Courses.Add(course);
            _dbContext.SaveChanges();


            return RedirectToAction("Index", "Home");
        }
    }
}
