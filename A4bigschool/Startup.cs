﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4bigschool.Startup))]
namespace A4bigschool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
